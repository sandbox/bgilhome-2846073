(function($) {

    Drupal.behaviors.ajaxClass = {
        attach: function(context, settings) {
            $('input[type="submit"]', context).once('ajax-class', function() {
               $(this).click(function() {
                   $(this).addClass('ajax-class-trigger');
                   $('body').addClass('ajax-waiting');
               });
            });
        }
    };

    // Add ajax loading classes to body - no need for a Drupal behavior since using jQuery events?
    $(document).ajaxStart(function() {
        $('body').addClass('ajax-waiting');     // can't use .ajax-progress, too much CSS to override
    });

    $(document).ajaxStop(function() {
        $('body').removeClass('ajax-waiting');
        $('body').addClass('ajax-complete');
    });

}(jQuery));

